﻿using AltaGamesTest.Data;
using AltaGamesTest.Ui;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using VSG.Singleton.Runtime;

namespace AltaGamesTest.Logic
{
    public class LevelManager : SingletonMonoBehaviour<LevelManager>
    {
        #region References

        [FoldoutGroup("References")][Required][Tooltip("Transform that represents player spawn point.")] 
        public Transform PlayerSpawnPoint;
        [FoldoutGroup("References")][Required][Tooltip("Transform that represents exit spawn point.")] 
        public Transform ExitSpawnPoint;

        #endregion

        #region Events

        [FoldoutGroup("Events")][Tooltip("Event that will be invoked when victory conditions are meet.")]
        public UnityEvent OnVictory = new();
        [FoldoutGroup("Events")][Tooltip("Event that will be invoked when game over conditions are meet.")]
        public UnityEvent OnGameOver = new();

        #endregion
        
        private void Start()
        {
            Initialize();
        }

        private void Initialize()
        {
            InitializePlayer(PlayerSpawnPoint);
            InitializeExit(ExitSpawnPoint);
            InitializeEnemies(PlayerSpawnPoint, ExitSpawnPoint);
            InitializeUi();
        }

        private void InitializeUi()
        {
            PlayerHealthBarPanel.Instance.Initialize(PlayerController.Instance.Size);
            VictoryPanel.Instance.Initialize();
            GameOverPanel.Instance.Initialize();
            PlayerController.Instance.OnPlayerSizeChanged.AddListener(TriggerHealthBarUpdate);
        }

        private void TriggerHealthBarUpdate(float playerSize)
        {
            PlayerHealthBarPanel.Instance.UpdatePlayerHealth(playerSize);
        }

        private void InitializeEnemies(Transform playerSpawnPoint, Transform exitSpawnPoint)
        {
            EnemyGenerator.Instance.RegenerateEnemies(playerSpawnPoint, exitSpawnPoint);
        }

        private void InitializeExit(Transform exitSpawnPoint)
        {
            Instantiate(PrefabsDatabase.Instance.Exit, exitSpawnPoint);
        }

        private void InitializePlayer(Transform playerSpawnPoint)
        {
            Instantiate(PrefabsDatabase.Instance.Player, playerSpawnPoint);
            PlayerController.Instance.Initialize();
            PlayerController.Instance.OnMoveComplete.AddListener(CheckForVictory);
            PlayerController.Instance.OnPlayerDead.AddListener(TriggerGameOver);
            PlayerController.Instance.OnProjectileGenerated.AddListener(SubscribeToProjectileEvents);
        }

        private void SubscribeToProjectileEvents(PlayerProjectileController controller)
        {
            controller.OnCollisionHit.AddListener(collisionObject =>
            {
                if (collisionObject != null)
                {
                    PlayerProjectileController projectile = PlayerController.Instance.Projectile.GetComponent<PlayerProjectileController>();
                    ExplosionManager.Instance.TriggerExplosion(projectile.transform.position, projectile.Size);
                }
                Destroy(PlayerController.Instance.Projectile.gameObject);
            });
        }

        private void CheckForVictory(Collision collision)
        {
            if (collision.gameObject.CompareTag("Exit"))
            {
                TriggerVictory();
            }
        }

        private void TriggerVictory()
        {
            PlayerController.Instance.PlayerHasControl = false;
            PlayerHealthBarPanel.Instance.Hide();
            VictoryPanel.Instance.UpdateScoreLabel(PlayerController.Instance.Size);
            VictoryPanel.Instance.Show();
            OnVictory.Invoke();
        }
        
        private void TriggerGameOver()
        {
            if (PlayerController.Instance.Projectile != null)
            {
                Destroy(PlayerController.Instance.Projectile.gameObject);   
            }
            Destroy(PlayerController.Instance.gameObject);
            PlayerHealthBarPanel.Instance.Hide();
            GameOverPanel.Instance.Show();
            OnGameOver.Invoke();
        }
    }
}