﻿using UnityEngine;

namespace AltaGamesTest.Logic
{
    [RequireComponent(typeof(Rigidbody))]
    public class EnemyController : MonoBehaviour
    {
        public Rigidbody Rigidbody;
        
        private void Awake()
        {
            if (Rigidbody == null)
            {
                Rigidbody = GetComponent<Rigidbody>();
            }
        }

        public void Die()
        {
            Destroy(gameObject);
        }
    }
}