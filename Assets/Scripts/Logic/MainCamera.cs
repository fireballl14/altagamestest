﻿using UnityEngine;
using UnityEngine.InputSystem;
using VSG.Singleton.Runtime;

namespace AltaGamesTest.Common
{
    [RequireComponent(typeof(Camera))]
    public class MainCamera : SingletonMonoBehaviour<MainCamera>
    {
        [SerializeField] private Camera _camera;
        private readonly Vector3 _vector3Zero = Vector3.zero;

        public Camera Camera
        {
            get
            {
                if (_camera == null)
                {
                    _camera = Camera.main;
                }
                return _camera;
            }
        }
        
        public Vector3 GetPointerWorldPosition()
        {
            Vector3 worldPosition = _vector3Zero;
            Vector2 pointerPosition = Pointer.current.position.ReadValue();
            Ray ray = Camera.ScreenPointToRay(pointerPosition);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                worldPosition = ray.GetPoint(hit.distance);
            }
            return worldPosition;
        }
    }
}