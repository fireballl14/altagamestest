using System.Collections;
using AltaGamesTest.Common;
using AltaGamesTest.Data;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using VSG.Singleton.Runtime;

namespace AltaGamesTest.Logic
{
    public class PlayerController : SingletonMonoBehaviour<PlayerController>, IMovable, IScalable
    {
        [Tooltip("Preview plane that shows target direction and player size.")]
        #region References
        [FoldoutGroup("References")] [Required] public Transform PreviewPlane;
        #endregion

        [Tooltip("Is player able to use controls.")]
        public bool PlayerHasControl = true;
        
        [Tooltip("Player target position that will be used to calculate shot and movement direction.")]
        public Vector3 Target;
        
        [Tooltip("Projectile object")]
        [ReadOnly][SerializeField] public PlayerProjectileController Projectile;
        
        [Tooltip("Projectile spawn offset distance.")]
        public float ProjectileSpawnDistance = 1f;

        [Tooltip("Minimum size of a player object that will trigger player death.")]
        public float DeathThreshold = 0.1f;
        
        #region Input Actions
        
        [Tooltip("Player action that will trigger updating target position.")]
        [FoldoutGroup("Input Actions")] public InputAction UpdateTargetPositionAction;
        
        [FormerlySerializedAs("PlayerAction")] [Tooltip("Player action that will trigger shooting sequence.")]
        [FoldoutGroup("Input Actions")] public InputAction ShootGenerationAction;
        
        #endregion
        
        #region Movable settings

        [HideInInspector][SerializeField] private float _movementSpeed = 10f;
        [FoldoutGroup("Movable settings")] [ShowInInspector][Tooltip("Player movement speed.")]
        public float MovementSpeed
        {
            get => _movementSpeed;
            private set => _movementSpeed = value;
        }
        
        [HideInInspector][SerializeField] private AnimationCurve _movementCurve = new();
        [FoldoutGroup("Movable settings")] [ShowInInspector][Tooltip("Player movement curve.")]
        public AnimationCurve MovementCurve
        {
            get => _movementCurve;
            private set => _movementCurve = value;
        }

        #endregion
        
        #region Scalable settings

        [HideInInspector][SerializeField] private float _size;
        [FoldoutGroup("Scalable settings")] [ShowInInspector][Tooltip("Current player size.")]
        public float Size
        {
            get => _size;
            private set
            {
                _size = value; 
                transform.localScale = _vector3One * _size;
                OnPlayerSizeChanged.Invoke(_size);
            }
        }

        [HideInInspector][SerializeField] private float _sizeDelta;
        [Tooltip("Player size delta in units per second.")]
        [FoldoutGroup("Scalable settings")][ShowInInspector][SuffixLabel("units per second", Overlay = true)]
        public float SizeDelta
        {
            get => _sizeDelta;
            private set => _sizeDelta = value;
        }
        
        #endregion

        #region Events

        [Tooltip("Event that will be invoked when new projectile is generated.")]
        [FoldoutGroup("Events")]public UnityEvent<PlayerProjectileController> OnProjectileGenerated = new ();
        
        [Tooltip("Event that will be invoked when player finished its move.")]
        [FoldoutGroup("Events")]public UnityEvent<Collision> OnMoveComplete = new ();

        [Tooltip("Event that will be invoked when player size is changed.")]
        [FoldoutGroup("Events")]public UnityEvent<float> OnPlayerSizeChanged = new ();
        
        [Tooltip("Event that will be invoked when size reaches "+ nameof(DeathThreshold) +".")]
        [FoldoutGroup("Events")]public UnityEvent OnPlayerDead = new ();
        
        #endregion
        
        private Coroutine _shrinkCoroutine;
        private Tween _movementTween;
        private readonly Vector3 _vector3One = Vector3.one;
        
        public void Initialize()
        {
            SubscribeToInput();
            Size = _size;
        }
        
        private void OnDestroy()
        {
            UnsubscribeFromInput();
            if (_shrinkCoroutine != null)
            {
                StopCoroutine(_shrinkCoroutine);
            }
            _movementTween.Kill();
        }

        private PlayerProjectileController GenerateProjectile(Vector3 target)
        {
            Vector3 projectileSpawnPoint = GetProjectileSpawnPoint(target);
            PlayerProjectileController projectile = Instantiate(PrefabsDatabase.Instance.Projectile, projectileSpawnPoint, Quaternion.identity)
                .GetComponent<PlayerProjectileController>();
            OnProjectileGenerated.Invoke(projectile);
            return projectile;
        }

        private Vector3 GetProjectileSpawnPoint(Vector3 target)
        {
            Vector3 playerPosition = transform.position;
            Vector3 targetDirection = target - playerPosition;
            Vector3 projectileSpawnPoint = playerPosition + targetDirection.normalized * ProjectileSpawnDistance;
            return projectileSpawnPoint;
        }

        public IEnumerator StartShrink()
        {
            while (true)
            {
                float delta = SizeDelta * Time.deltaTime;
                ChangeSize(delta);
                SetSizeOfPreviewPlane(Size);
                if (Size < DeathThreshold)
                {
                    OnPlayerDead.Invoke();
                }
                yield return null;
            }
            // ReSharper disable once IteratorNeverReturns
        }

        public void StopShrink()
        {
            if (_shrinkCoroutine != null)
            {
                StopCoroutine(_shrinkCoroutine);
                _shrinkCoroutine = null;
            }
        }
        
        private void SetTargetToPointerPosition()
        {
            Vector3 pointerWorldPosition = MainCamera.Instance.GetPointerWorldPosition();
            Target = new (pointerWorldPosition.x, transform.position.y, pointerWorldPosition.z);
        }

        private void RotatePreviewPlaneToTarget(Vector3 target)
        {
            PreviewPlane.rotation = Quaternion.LookRotation(target - transform.position, Vector3.up);
        }

        private void SetSizeOfPreviewPlane(float size)
        {
            PreviewPlane.localScale = new(size, 1, 1);
        }
      
        #region InputHandlers
        
        private void SubscribeToInput()
        {
            ShootGenerationAction.performed += OnShootGenerationActionPerformed;
            ShootGenerationAction.canceled += OnShootGenerationActionCanceled;
            UpdateTargetPositionAction.performed += OnUpdateTargetPositionActionPerformed;
            ShootGenerationAction.Enable();
            UpdateTargetPositionAction.Enable();
        }


        
        private void UnsubscribeFromInput()
        {
            ShootGenerationAction.performed -= OnShootGenerationActionPerformed;
            ShootGenerationAction.canceled -= OnShootGenerationActionCanceled;
            UpdateTargetPositionAction.performed -= OnUpdateTargetPositionActionPerformed;
            ShootGenerationAction.Disable();
            UpdateTargetPositionAction.Disable();
        }
        
        private void OnUpdateTargetPositionActionPerformed(InputAction.CallbackContext obj)
        {
            if (PlayerHasControl == false)
            {
                return;
            }
            SetTargetToPointerPosition();
            RotatePreviewPlaneToTarget(Target);
        }
        
        private void OnShootGenerationActionPerformed(InputAction.CallbackContext context)
        {
            if (PlayerHasControl == false)
            {
                return;
            }
            PreviewPlane.gameObject.SetActive(true);
            _shrinkCoroutine = StartCoroutine(StartShrink());
            SetTargetToPointerPosition();
            RotatePreviewPlaneToTarget(Target);
            Projectile = GenerateProjectile(Target);
            Size -= Projectile.Size;
            Projectile.Initialize(ProjectileSpawnDistance);
            Projectile.OnCollisionHit.AddListener(_ => MoveTo(Target, MovementCurve));
        }
        
        private void OnShootGenerationActionCanceled(InputAction.CallbackContext context)
        {
            PreviewPlane.gameObject.SetActive(false);
            if (Projectile == null)
            {
                return;
            }
            StopShrink();
            PlayerHasControl = false;
        }
        
        #endregion
      
        #region IScalable
        
        public void ChangeSize(float delta)
        {
            Size += delta;
        }
        
        #endregion
        
        #region IMovable

        private void OnCollisionEnter(Collision col)
        {
            PlayerHasControl = true;
            _movementTween.Kill();
            OnMoveComplete.Invoke(col);
        }

        public void MoveTo(Vector3 target, AnimationCurve movementCurve)
        {
            Vector3 projectilePosition = transform.position;
            Vector3 finalMovePoint = target;
            finalMovePoint = new(finalMovePoint.x, projectilePosition.y, finalMovePoint.z);
            _movementTween = transform.DOMove(finalMovePoint, MovementSpeed);
            _movementTween.SetSpeedBased(true);
            _movementTween.SetEase(movementCurve);
            _movementTween.OnComplete(() =>
            {
                PlayerHasControl = true;
                OnMoveComplete.Invoke(null);
            });
        }

        #endregion
    }
}