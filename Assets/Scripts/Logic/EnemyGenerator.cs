using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using VSG.Singleton.Runtime;
using Random = UnityEngine.Random;

namespace AltaGamesTest.Logic
{
    public class EnemyGenerator : SingletonMonoBehaviour<EnemyGenerator>
    {
        #region References

        [FoldoutGroup("References")][Required] public EnemiesPool EnemyPool;
        [FoldoutGroup("References")][Required] public Transform ActiveEnemiesHolder;
   
        #endregion       
        
        #region Generator Settings

        [Header("Generator settings")] 
        [Tooltip("Number of enemies to generate")]
        public uint NumberOfEnemies = 100;
        [Tooltip("Generation radius")]
        public float Radius = 1;
        [InfoBox("Note: Seed is set to zero, this will auto-generate random seed!", "@" + nameof(Seed) + "==0")]
        [Tooltip("Seed that will be used for next generation")]
        public int Seed;
        
        [Tooltip("Currently used seed")]
        [ReadOnly][SerializeField] private int _usedSeed;

        [Tooltip("Height on which enemies spawn")]
        public float SpawnHeight;
        
        [Tooltip("How big is no spawn area around player")]
        public float PlayerSafeRadius;
        
        [Tooltip("How big is no spawn area around exit")]
        public float ExitSafeRadius;
        
        [Tooltip("How big is no spawn area around each enemy")]
        public float EnemySafeRadius;

        [Tooltip("How many times generator will try to find empty spot for new enemy, until it will give up")]
        public uint GenerationThreshold = 1000;
        #endregion
        
        [ListDrawerSettings(IsReadOnly = true)]
        [ShowInInspector] private List<GameObject> _activeEnemies = new();

        public ReadOnlyCollection<GameObject> ActiveEnemies => new (_activeEnemies);
        
        private readonly Vector2 _vector2Zero = Vector2.zero;


        [Button]
        public void RegenerateEnemies()
        {
            RegenerateEnemies(LevelManager.Instance.PlayerSpawnPoint, LevelManager.Instance.ExitSpawnPoint);
        }
        
        public void RegenerateEnemies(Transform playerSpawnPoint, Transform exitSpawnPoint)
        {
            Vector3 midpoint = (exitSpawnPoint.position + playerSpawnPoint.position) / 2f;
            RegenerateEnemies(NumberOfEnemies, midpoint, Radius, playerSpawnPoint, exitSpawnPoint);
        }

        public void RegenerateEnemies(uint numberOfEnemies, Vector2 spawnPoint, float radius, Transform playerSpawnPoint, Transform exitSpawnPoint)
        {
            DestroyAllEnemies();
            GenerateEnemies(numberOfEnemies, spawnPoint, radius, GenerationThreshold, playerSpawnPoint, exitSpawnPoint);
        }

        private void GenerateEnemies(uint numberOfEnemies, Vector2 spawnPoint, float radius, uint generationThreshold, Transform playerSpawnPoint, Transform exitSpawnPoint)
        {
            _usedSeed = Seed == 0 ? Random.Range(int.MinValue, int.MaxValue) : Seed;
            Random.InitState(_usedSeed);
            if (GenerateEnemyCoordinates(numberOfEnemies, spawnPoint, radius, generationThreshold, playerSpawnPoint, exitSpawnPoint, 
                    out List<Vector2> newEnemiesCoordinates) == false)
            {
                return;
            }
            foreach (Vector2 enemyCoordinates in newEnemiesCoordinates)
            {
                EnemyPool.Pool.Get(out GameObject newEnemyInstance);
                newEnemyInstance.transform.position = new (enemyCoordinates.x, SpawnHeight, enemyCoordinates.y);
                newEnemyInstance.transform.SetParent(ActiveEnemiesHolder);
                _activeEnemies.Add(newEnemyInstance);
            }
        }

        private bool GenerateEnemyCoordinates(uint numberOfEnemies, Vector2 spawnPoint, float radius, uint generationThreshold
            ,Transform playerSpawnPoint, Transform exitSpawnPoint, out List<Vector2> newEnemiesCoordinates)
        {
            newEnemiesCoordinates = new();
            for (int i = 0; i < numberOfEnemies; i++)
            {
                uint trysCount = 0;
                Vector2 newEnemyCoordinates = _vector2Zero;
                do
                {
                    if (trysCount > generationThreshold)
                    {
                        Debug.LogWarning("Could not find empty spot for an enemy," +
                                       " try lowering number of enemies or reducing safe radius.");
                        
                        break;
                    }
                    newEnemyCoordinates = Random.insideUnitSphere;
                    newEnemyCoordinates *= radius;
                    newEnemyCoordinates += spawnPoint;
                    trysCount++;
                } while (IsOverlappingOtherObjects(newEnemyCoordinates, 
                             newEnemiesCoordinates, playerSpawnPoint, exitSpawnPoint) == true);
                if (newEnemyCoordinates != _vector2Zero)
                {
                    newEnemiesCoordinates.Add(newEnemyCoordinates);    
                }
            }
            return true;
        }

        private bool IsOverlappingOtherObjects(Vector2 newEnemyCoordinates, IEnumerable<Vector2> newEnemiesCoordinates,
            Transform playerSpawnPoint, Transform exitSpawnPoint)
        {
            if (Vector2.Distance(newEnemyCoordinates, playerSpawnPoint.position) < PlayerSafeRadius)
            {
                return true;
            }
            if (Vector2.Distance(newEnemyCoordinates, exitSpawnPoint.position) < ExitSafeRadius)
            {
                return true;
            }
            if (newEnemiesCoordinates.Any(otherEnemiesCoordinates => 
                    Vector2.Distance(newEnemyCoordinates, otherEnemiesCoordinates) < EnemySafeRadius))
            {
                return true;
            }
            return false;
        }

        [Button("Destroy All Enemies")]
        private void DestroyAllEnemies()
        {
            CleanUp(_activeEnemies);
        }
        
        private void CleanUp(List<GameObject> activeEnemies)
        {
            List<GameObject> enemiesToRemove = new (activeEnemies);
            foreach (GameObject activeEnemy in enemiesToRemove)
            {
                EnemyPool.Pool.Release(activeEnemy);
                _activeEnemies.Remove(activeEnemy);
            }
        }
    }
}