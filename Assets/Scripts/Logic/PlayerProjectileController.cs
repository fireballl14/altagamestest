﻿using System.Collections;
using AltaGamesTest.Common;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace AltaGamesTest.Logic
{
    public class PlayerProjectileController : MonoBehaviour, IMovable, IScalable
    {
        [Tooltip("Projectile target position that will be used to calculate movement direction.")]
        public Vector3 Target;

        #region Input Actions
        [Tooltip("Player action that will trigger updating target position.")]
        [FoldoutGroup("Input Actions")] public InputAction UpdateTargetPositionAction;

        [Tooltip("Player action that will trigger movement when it's finished.")]
        [FoldoutGroup("Input Actions")] public InputAction EndGrowPhaseAction;
        
        #endregion

        #region Movable settings
        
        [HideInInspector][SerializeField] private float _movementSpeed = 10f;
        [FoldoutGroup("Movable settings")] [ShowInInspector][Tooltip("Projectile movement speed.")]
        public float MovementSpeed
        {
            get => _movementSpeed;
            private set => _movementSpeed = value;
        }
        
        [HideInInspector][SerializeField] private AnimationCurve _movementCurve = new();
        [FoldoutGroup("Movable settings")] [ShowInInspector][Tooltip("Projectile movement curve.")]
        public AnimationCurve MovementCurve
        {
            get => _movementCurve;
            private set => _movementCurve = value;
        }
        
        #endregion
        
        #region Scalable settings

        [HideInInspector][SerializeField] private float _size;
        [FoldoutGroup("Scalable settings")] [ShowInInspector][Tooltip("Current projectile size.")]
        public float Size
        {
            get => _size;
            private set
            {
                _size = value;
                transform.localScale = _vector3One * _size;
            }
        }

        [HideInInspector][SerializeField] private float _sizeDelta;
        [Tooltip("Projectile size delta in units per second.")]
        [FoldoutGroup("Scalable settings")][ShowInInspector][SuffixLabel("units per second", Overlay = true)]
        public float SizeDelta
        {
            get => _sizeDelta;
            private set => _sizeDelta = value;
        }
        
        #endregion
        
        #region Events 

        [Tooltip("Event that will be invoked when projectile hit something or at an end of it's lifetime.")]
        [FoldoutGroup("Events")] public UnityEvent<GameObject> OnCollisionHit = new ();
        
        #endregion
        
        private float _orbitDistance;
        
        private Coroutine _growCoroutine;
        private Tween _movementTween;
        private readonly Vector3 _vector3One = Vector3.one;
        
        public void Initialize(float orbitDistance)
        {
            _orbitDistance = orbitDistance;
            Size = _size;
            SetTargetToPointerPosition();
            SubscribeToInput();
            _growCoroutine = StartCoroutine(StartGrow());
        }

        private void OnDestroy()
        {
            UnsubscribeFromInput();
            if (_growCoroutine != null)
            {
                StopCoroutine(_growCoroutine);
            }
            _movementTween.Kill();
        }

        public IEnumerator StartGrow()
        {
            while (true)
            {
                float delta = SizeDelta * Time.deltaTime;
                ChangeSize(delta);
                yield return null;
            }
            // ReSharper disable once IteratorNeverReturns
        }

        public void StopGrow()
        {
            if (_growCoroutine != null)
            {
                StopCoroutine(_growCoroutine);
                _growCoroutine = null;
            }
        }
        
        private void SetTargetToPointerPosition()
        {
            Vector3 pointerWorldPosition = MainCamera.Instance.GetPointerWorldPosition();
            Target = new (pointerWorldPosition.x, transform.position.y, pointerWorldPosition.z);
        }

        private void UpdateOrbitPosition(Vector3 target, Vector3 orbitCenter, float orbitDistance)
        {
            Vector3 targetDirection = target - orbitCenter;
            Vector3 orbitPosition = orbitCenter + targetDirection.normalized * orbitDistance;
            transform.position = orbitPosition;
        }
        
        #region InputHandlers
        private void SubscribeToInput()
        {
            EndGrowPhaseAction.canceled += OnEndGrowPhaseActionPerformed;
            UpdateTargetPositionAction.performed += OnUpdateTargetPositionActionPerformed;
            EndGrowPhaseAction.Enable();
            UpdateTargetPositionAction.Enable();
        }

        private void UnsubscribeFromInput()
        {
            EndGrowPhaseAction.canceled -= OnEndGrowPhaseActionPerformed;
            UpdateTargetPositionAction.performed -= OnUpdateTargetPositionActionPerformed;
            EndGrowPhaseAction.Disable();
            UpdateTargetPositionAction.Disable();
        }
        
        private void OnUpdateTargetPositionActionPerformed(InputAction.CallbackContext obj)
        {
            if (PlayerController.Instance.PlayerHasControl == false)
            {
                return;
            }
            SetTargetToPointerPosition();
            UpdateOrbitPosition(Target, PlayerController.Instance.transform.position, _orbitDistance);
        }
        
        private void OnEndGrowPhaseActionPerformed(InputAction.CallbackContext context)
        {
            if (context.canceled == false)
            {
                return;
            }
            StopGrow();
            MoveTo(Target, MovementCurve);
            UnsubscribeFromInput();
        }
        
        #endregion
      
        #region IScalable
        
        public void ChangeSize(float delta)
        {
            Size += delta;
        }
        
        #endregion
        
        #region IMovable
        
        private void OnTriggerStay(Collider col)
        {
            if (_movementTween == null || _movementTween.IsPlaying() == false)
            {
                return;
            }
            _movementTween.Kill();
            OnCollisionHit.Invoke(col.transform.gameObject);
        }

        public void MoveTo(Vector3 target, AnimationCurve movementCurve)
        {
            Vector3 projectilePosition = transform.position;
            Vector3 moveDirection = target - projectilePosition;
            Vector3 moveDirectionNormalized = moveDirection.normalized;
            Vector3 finalMovePoint = projectilePosition + moveDirectionNormalized * 50;
            finalMovePoint = new(finalMovePoint.x, projectilePosition.y, finalMovePoint.z);
            _movementTween = transform.DOMove(finalMovePoint, MovementSpeed);
            _movementTween.SetSpeedBased(true);
            _movementTween.SetEase(movementCurve);
            _movementTween.OnComplete(() =>
            {
                OnCollisionHit.Invoke(null);
            });
        }

        #endregion
    }
}