﻿using System.Collections.Generic;
using System.Linq;
using AltaGamesTest.Data;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using VSG.Singleton.Runtime;

namespace AltaGamesTest.Logic
{
    public class ExplosionManager : SingletonMonoBehaviour<ExplosionManager>
    {
        #region Explosion settings
        
        [FoldoutGroup("Explosion settings")][SuffixLabel("Just for fun! ;)")] public bool UseExplosionForce = false;
        [FoldoutGroup("Explosion settings")] public float PowerMultiplier = 1;
        [FoldoutGroup("Explosion settings")] public float RadiusMultiplier = 2;
        [FoldoutGroup("Explosion settings")][SuffixLabel("seconds", Overlay = true)] public float ExplosionObjectLifeTime = 1;
        [FoldoutGroup("Explosion settings")] public AnimationCurve ExplosionObjectScaleCurve = new ();
        
        #endregion
        
        private readonly Vector3 _vector3One = Vector3.one;

        public void TriggerExplosion(Vector3 target, float radius)
        {
            GameObject explosion = Instantiate(PrefabsDatabase.Instance.Explosion, target, Quaternion.identity);
            explosion.transform.DOScale(_vector3One * radius * RadiusMultiplier, ExplosionObjectLifeTime).SetEase(ExplosionObjectScaleCurve);
            Destroy(explosion, ExplosionObjectLifeTime + Time.fixedDeltaTime);
            List<EnemyController> enemies = FindEnemiesInRadius(target, RadiusMultiplier * radius);
            foreach (EnemyController enemy in enemies)
            {
                if (UseExplosionForce == true)
                {
                    enemy.Rigidbody.AddExplosionForce(PowerMultiplier * radius, new Vector3(target.x, 0, target.z), RadiusMultiplier * radius);    
                }
                else
                {
                    enemy.Die();    
                }
            }
        }

        private List<EnemyController> FindEnemiesInRadius(Vector3 target, float radius)
        {
            // ReSharper disable once Unity.PreferNonAllocApi
            RaycastHit[] hits= Physics.SphereCastAll(target, radius, Vector3.up, 0, 1 << LayerMask.NameToLayer("Enemy"));
            return hits.Select(enemyHit => enemyHit.transform.GetComponent<EnemyController>()).ToList();
        }
    }
}