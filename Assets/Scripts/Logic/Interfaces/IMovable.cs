﻿using UnityEngine;

namespace AltaGamesTest.Logic
{
    public interface IMovable
    {
        public float MovementSpeed { get; }
        public AnimationCurve MovementCurve { get; }
        public void MoveTo(Vector3 target, AnimationCurve movementCurve);
    }
}