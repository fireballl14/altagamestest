﻿namespace AltaGamesTest.Logic
{
    public interface IScalable
    {
        public float Size { get; }
        public float SizeDelta { get; }
        public void ChangeSize(float delta);
    }
}