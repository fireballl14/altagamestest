﻿using AltaGamesTest.Data;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Pool;
using VSG.Singleton.Runtime;

namespace AltaGamesTest.Logic
{
    public class EnemiesPool : SingletonMonoBehaviour<EnemiesPool>
    {
        [Tooltip("Maximum pool size")]
        public int MaxPoolSize = 100;
        
        [Tooltip("Collection checks will throw errors if we try to release an item that is already in the pool.")]
        public bool CollectionChecks = true;
        
        public IObjectPool<GameObject> Pool;

        private static readonly Vector3 Vector3Zero = Vector3.zero;
        
        protected override void Awake()
        {
            base.Awake();
            Pool = new ObjectPool<GameObject>(CreatePooledItem, OnTakeFromPool,
                OnReturnedToPool, OnDestroyPoolObject, CollectionChecks, 100, MaxPoolSize);
        }

        private GameObject CreatePooledItem()
        {
            GameObject newEnemyObject = Instantiate(PrefabsDatabase.Instance.Enemy);
            newEnemyObject.SetActive(false);
            return newEnemyObject;
        }

        private static void OnDestroyPoolObject(GameObject obj)
        {
            Destroy(obj);
        }

        private static void OnReturnedToPool(GameObject obj)
        {
            obj.transform.SetParent(Instance.transform);
            obj.transform.localPosition = Vector3Zero;
            obj.SetActive(false);
        }

        private static void OnTakeFromPool(GameObject obj)
        {
            obj.SetActive(true);
        }
        
        [Button]
        public void Clear()
        {
            Pool.Clear();
        }
    }
}