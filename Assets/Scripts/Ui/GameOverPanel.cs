﻿using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using VSG.UiPanels.Runtime;

namespace AltaGamesTest.Ui
{
    public class GameOverPanel : UiPanel<GameOverPanel>
    {
        #region References

        [FoldoutGroup("References")][Required]public Button RestartButton;
        
        #endregion
        
        public void Initialize()
        {
            RestartButton.onClick.AddListener(Restart);
        }
        
        private void Restart()
        {
            SceneManager.LoadScene(0);
        }
    }
}