﻿using System.Globalization;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using VSG.UiPanels.Runtime;

namespace AltaGamesTest.Ui
{
    public class VictoryPanel : UiPanel<VictoryPanel>
    {
        #region References

        [FoldoutGroup("References")][Required]public TMP_Text ScoreLabel;
        [FoldoutGroup("References")][Required]public Button RestartButton;

        #endregion
        
        public void Initialize()
        {
            RestartButton.onClick.AddListener(Restart);
        }

        public void UpdateScoreLabel(float playerSize)
        {
            ScoreLabel.text = "Score: "+ Mathf.RoundToInt(playerSize * 1000).ToString(CultureInfo.InvariantCulture);
        }
        
        private void Restart()
        {
            SceneManager.LoadScene(0);
        }

    }
}