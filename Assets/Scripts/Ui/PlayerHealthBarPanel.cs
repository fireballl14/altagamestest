﻿using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VSG.UiPanels.Runtime;

namespace AltaGamesTest.Ui
{
    public class PlayerHealthBarPanel : UiPanel<PlayerHealthBarPanel>
    {
        #region References

        [FoldoutGroup("References")][Required]public Scrollbar Bar;
        [FoldoutGroup("References")] [Required] public TMP_Text Label;
        
        #endregion
        
        private float _maxSize;
        
        public void Initialize(float playerSize)
        {
            _maxSize = playerSize;
            UpdatePlayerHealth(playerSize);
        }
        public void UpdatePlayerHealth(float playerSize)
        {
            if (Mathf.Approximately(playerSize ,0) == false)
            {
                Bar.size = playerSize / _maxSize;
                Label.SetText(Mathf.RoundToInt(playerSize * 100) + " / " + Mathf.RoundToInt(_maxSize * 100f));
            }
            else
            {
                Bar.size = 0;
                Label.SetText("Dead");
            }
        }
    }
}