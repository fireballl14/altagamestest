﻿using UnityEngine;
using VSG.Singleton.Runtime;

namespace AltaGamesTest.Data
{
    [CreateAssetMenu]
    public class PrefabsDatabase : SingletonScriptableObject<PrefabsDatabase>
    {
        public GameObject Player;
        public GameObject Enemy;
        public GameObject Projectile;
        public GameObject Explosion;
        public GameObject Exit;
    }
}